#!/usr/bin/env python3
''' Written by jared.vasquez@yale.edu '''

from keras.models import load_model
import matplotlib.pyplot as plt
import numpy as np
import copy
import cv2
import os
import serial

serial_on = 1
try:
    ser = serial.Serial('/dev/ttyUSB0', 9600)
except:
    serial_on = 0

dataColor = (0, 255, 0)
font = cv2.FONT_HERSHEY_SIMPLEX
fx, fy, fh = 10, 50, 45
takingData = 0
className = 'NONE'
count = 0
showMask = 0


classes = 'NONE ONE TWO THREE FOUR FIVE'.split()


def initClass(name):
    global className, count
    className = name
    os.system('mkdir -p data/%s' % name)
    count = len(os.listdir('data/%s' % name))


def binaryMask(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.GaussianBlur(img, (7, 7), 3)
    img = cv2.adaptiveThreshold(
        img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)
    ret, new = cv2.threshold(
        img, 25, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    return new


def printThreshold(thr):
    print("! Changed threshold to " + str(thr))


def removeBG(frame, bgimage, threshold):

    diff = np.abs(cv2.subtract(bgimage, frame))
    diff_gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
    #blur = cv2.GaussianBlur(diff, (blurValue, blurValue), 0)
    ret, diff_thres = cv2.threshold(diff_gray, threshold, 255, 0)

    kernel = np.ones((3, 3), np.uint8)
    diff = cv2.erode(diff_thres, kernel, iterations=1)
    res = cv2.bitwise_and(frame, frame, mask=diff)

    cv2.imshow('diff', diff)

    return res


def main():
    global font, size, fx, fy, fh
    global takingData, dataColor
    global className, count
    global showMask

    model = load_model('model_6cat.h5')

    isBgCaptured = 0
    bgimage = 0
    threshold = 10

    x0, y0, width = 150, 150, 300

    cam = cv2.VideoCapture(0)
    cv2.namedWindow('Original', cv2.WINDOW_NORMAL)

    wait = cv2.imread('wait.jpg')
    img_bot = wait
    rock = cv2.imread('rock.jpg')
    paper = cv2.imread('paper.jpg')
    scissor = cv2.imread('scissor.jpg')

    cv2.namedWindow('trackbar')
    cv2.createTrackbar('trh1', 'trackbar', threshold, 100, printThreshold)

    while True:
        # Get camera frame
        ret, frame = cam.read()
        frame = cv2.flip(frame, 1)  # mirror
        #(h, w) = frame.shape[:2]
        #center = (w / 2, h / 2)
        #M = cv2.getRotationMatrix2D(center, 180, 1.0)
        #frame = cv2.warpAffine(frame, M, (w, h))

        threshold = cv2.getTrackbarPos('trh1', 'trackbar')
        if (isBgCaptured == 1):
            frame = removeBG(frame, bgimage, threshold)
        window = copy.deepcopy(frame)
        cv2.rectangle(window, (x0, y0), (x0 + width -
                                         1, y0 + width - 1), dataColor, 12)

        # draw text
        if takingData:
            dataColor = (0, 250, 0)
            cv2.putText(window, 'Data Taking: ON', (fx, fy),
                        font, 1.2, dataColor, 2, 1)
        else:
            dataColor = (0, 0, 250)
            cv2.putText(window, 'Data Taking: OFF',
                        (fx, fy), font, 1.2, dataColor, 2, 1)
        cv2.putText(window, 'Class Name: %s (%d)' % (className, count),
                    (fx, fy + fh), font, 1.0, (245, 210, 65), 2, 1)

        # get region of interest
        roi = frame[y0:y0 + width, x0:x0 + width]
        roi = binaryMask(roi)

        # apply processed roi in frame
        if showMask:
            window[y0:y0 + width, x0:x0 +
                   width] = cv2.cvtColor(roi, cv2.COLOR_GRAY2BGR)

        # take data or apply predictions on ROI
        if takingData:
            cv2.imwrite('data/{0}/{0}_{1}.png'.format(className, count), roi)
            count += 1
        else:
            img = np.float32(roi) / 255.

            img = cv2.resize(img, (100, 100))
            img = np.expand_dims(img, axis=0)
            img = np.expand_dims(img, axis=-1)

            pred = classes[np.argmax(model.predict(img)[0])]
            cv2.putText(window, 'Prediction: %s' % (pred),
                        (fx, fy + 2 * fh), font, 1.0, (245, 210, 65), 2, 1)

            if(serial_on):
                if pred == 'ONE':
                    ser.write(bytes('3', 'utf-8'))
                elif pred == 'TWO':
                    ser.write(bytes('4', 'utf-8'))

            if pred == 'ONE' or pred == 'TWO' or pred == 'THREE':
                img_bot = rock
                if (serial_on == 1):
                    ser.write(bytes('0', 'utf-8'))
            elif pred == 'NONE':
                img_bot = paper
                if (serial_on == 1):
                    ser.write(bytes('5', 'utf-8'))
            elif pred == 'FIVE' or pred == 'FOUR':
                img_bot = scissor
                if (serial_on == 1):
                    ser.write(bytes('2', 'utf-8'))
            else:
                img_bot = wait

            # use below for demoing purposes
            # cv2.putText(window, 'Prediction: %s' % (pred), (x0,y0-25), font, 1.0, (255,0,0), 2, 1)

        # show the window
        cv2.imshow('Original', window)
        cv2.imshow('data', roi)
        cv2.imshow('bot', img_bot)
        # Keyboard inputs
        key = cv2.waitKey(10) & 0xff

        # use q key to close the program
        if key == ord('q'):
            break

        # Toggle data taking
        elif key == ord('s'):
            takingData = not takingData

        elif key == ord('b'):
            showMask = not showMask

        # Toggle class
        elif key == ord('0'):
            initClass('NONE')
        elif key == ord('`'):
            initClass('NONE')  # because 0 is on other side of keyboard
        elif key == ord('1'):
            initClass('ONE')
        elif key == ord('2'):
            initClass('TWO')
        elif key == ord('3'):
            initClass('THREE')
        elif key == ord('4'):
            initClass('FOUR')
        elif key == ord('5'):
            initClass('FIVE')
        elif key == ord('z'):  # press 'z' to capture the background
            _, bgimage = cam.read()
            bgimage = cv2.flip(bgimage, 1)
            #(h, w) = bgimage.shape[:2]
            #center = (w / 2, w / 2)
            #M = cv2.getRotationMatrix2D(center, 180, 1.0)
            #bgimage = cv2.warpAffine(bgimage, M, (w, h))

            # bgimage = cv2.cvtColor(bgimage, cv2.COLOR_BGR2GRAY)
            print(bgimage.shape)
            isBgCaptured = 1
            print('!!!Background Captured!!!')
        elif key == ord('r'):
            isBgCaptured = 0

        # adjust the size of window
        elif key == ord('o'):
            width = width - 5
        elif key == ord('p'):
            width = width + 5

        # adjust the position of window
        elif key == ord('i'):
            y0 = max((y0 - 5, 0))
        elif key == ord('k'):
            y0 = min((y0 + 5, window.shape[0] - width))
        elif key == ord('j'):
            x0 = max((x0 - 5, 0))
        elif key == ord('l'):
            x0 = min((x0 + 5, window.shape[1] - width))

    cam.release()


if __name__ == '__main__':
    initClass('NONE')
    main()
