from PIL import Image
import os
import sys


def resize(path):
    dirs = os.listdir(path)
    for item in dirs:
        if os.path.isfile(path + item):
            print(item[-3:])
            if (item[-3:] == "png"):
                im = Image.open(path + item)
                f, e = os.path.splitext(path + item)
                imResize = im.resize((100, 100), Image.ANTIALIAS)
                imResize.save(path + "s_" + item, quality=90)
                os.remove(path + item)
        else:
            resize(path + item + "/")


resize("train/")
